import { configureStore } from '@reduxjs/toolkit';
import gameSlice from '../Features/gameSlice';

export const store = configureStore(
    {
        reducer: {
            game: gameSlice,
        },
    },

    window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__(),
);
