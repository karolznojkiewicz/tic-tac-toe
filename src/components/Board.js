import React from 'react';
import styled from 'styled-components';
import Square from './Square';

const SquareBox = styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-template-rows: repeat(3, 1fr);
    min-width: 100%;
    min-height: 100%;
`;

const Board = ({ squares, onClick }) => (
    <SquareBox>
        {squares.map((square, i) => (
            <Square value={square} key={i} handleClick={() => onClick(i)} />
        ))}
    </SquareBox>
);

export default Board;
