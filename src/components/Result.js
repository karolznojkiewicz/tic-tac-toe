import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { winnerPlayerX, winnerPlayerO } from '../Features/gameSlice';
import styled from 'styled-components';

const ResultWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 500px;
    margin-top: 10px;
    margin-bottom: 25px;
`;
const ResultBox = styled.div`
    font-size: 1.4em;
    width: 40%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background-color: #fff;
    border-radius: 30px;
    padding: 10px 20px;
`;
const Between = styled.span`
    font-size: 2em;
    font-weight: bold;
`;

const Result = ({ winner }) => {
    const resultX = useSelector((state) => state.game.resultX);
    const resultO = useSelector((state) => state.game.resultO);
    const dispatch = useDispatch();

    useEffect(() => {
        if (winner === 'X') {
            dispatch(winnerPlayerX());
        }
        if (winner === 'O') {
            dispatch(winnerPlayerO());
        }
    }, [winner]);

    return (
        <ResultWrapper>
            <ResultBox>
                <span>X</span>
                <span>{resultX}</span>
            </ResultBox>
            <Between>:</Between>
            <ResultBox>
                <span>O</span>
                <span>{resultO}</span>
            </ResultBox>
        </ResultWrapper>
    );
};

export default Result;
