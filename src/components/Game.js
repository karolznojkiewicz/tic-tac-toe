import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { newGame, next } from '../Features/gameSlice';
import Board from './Board';
import Result from './Result';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 60%;
    height: 60%;
`;
const Button = styled.button`
    border: 4px solid #32a192;
    border-radius: 7px;
    padding: 7px 12px;
    background-color: transparent;
    text-transform: uppercase;
    font-size: 1.4em;
    margin: 10px 0;
`;

const calculateWinner = (squares) => {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (
            squares[a] &&
            squares[a] === squares[b] &&
            squares[a] === squares[c]
        ) {
            return squares[a];
        }
    }

    return null;
};

const Game = () => {
    //Selector
    const board = useSelector((state) => state.game.board);
    const isNext = useSelector((state) => state.game.isNext);
    //Dispatch actions
    const dispatch = useDispatch();

    const winner = calculateWinner(board);

    const handleClick = (i) => {
        const copyBoard = [...board];
        if (winner || copyBoard[i]) return;

        copyBoard[i] = isNext ? 'X' : 'O';
        dispatch(next(copyBoard));
    };

    const startGame = () => (
        <Button onClick={() => dispatch(newGame())}>Start game</Button>
    );

    return (
        <Wrapper>
            <h3>Result</h3>
            <Result winner={winner} />
            <p>
                {winner
                    ? 'Winner ' + winner
                    : 'Next player: ' + (isNext ? 'X' : 'O')}
            </p>
            <Board squares={board} onClick={handleClick} />

            {startGame()}
        </Wrapper>
    );
};

export default Game;
