import React from 'react';
import styled from 'styled-components';

const greenDarkBorder = '6px solid #32a192';

const Button = styled.button`
    background-color: transparent;
    font-size: 35px;
    width: 100%;
    height: 100%;
    border: none;
    &:focus {
        outline: none;
    }
    &:nth-child(1) {
        border-right: ${greenDarkBorder};
        border-bottom: ${greenDarkBorder};
    }
    &:nth-child(2) {
        border-right: ${greenDarkBorder};
        border-bottom: ${greenDarkBorder};
    }
    &:nth-child(3) {
        border-bottom: ${greenDarkBorder};
    }
    &:nth-child(4) {
        border-right: ${greenDarkBorder};
        border-bottom: ${greenDarkBorder};
    }
    &:nth-child(5) {
        border-right: ${greenDarkBorder};
        border-bottom: ${greenDarkBorder};
    }
    &:nth-child(6) {
        border-bottom: ${greenDarkBorder};
    }

    &:nth-child(7) {
        border-right: ${greenDarkBorder};
    }
    &:nth-child(8) {
        border-right: ${greenDarkBorder};
    }
`;

const Square = ({ value, handleClick }) => (
    <Button onClick={handleClick}>{value}</Button>
);

export default Square;
