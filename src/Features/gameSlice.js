import { createSlice } from '@reduxjs/toolkit';

const gameSlice = createSlice({
    name: 'game',
    initialState: {
        board: Array(9).fill(null),
        isNext: true,
        resultX: 0,
        resultO: 0,
    },
    reducers: {
        newGame: (state) => {
            state.board = Array(9).fill(null);
        },
        next: (state, action) => {
            state.board = action.payload;
            state.isNext = !state.isNext;
        },
        winnerPlayerX: (state) => {
            state.resultX += 1;
        },
        winnerPlayerO: (state) => {
            state.resultO += 1;
        },
    },
});

export const {
    newGame,
    next,
    winnerPlayerX,
    winnerPlayerO,
} = gameSlice.actions;

export default gameSlice.reducer;
